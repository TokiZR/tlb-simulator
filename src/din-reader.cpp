#include "din-reader.h"

#include <stdint.h>

/* Jump over the remainder of the current line on a stream. */
void fflushl(FILE * f) {
	int c;
	do {
		c = fgetc(f);
	} while (c != '\n' && c > 0);
}

void din_reader_read(FILE * din, Simulation * sim) {
	Instruction ins;
	uint32_t addr;
	while (fscanf(din, "%d %x", (unsigned *) &ins, &addr) == 2) {
		/* Skip remainder of the line. */
		fflushl(din);
		/* escape record (treated as unknown access type), to us it is just
		 * another read */
		if(ins == 3) ins = INSTRUCTION_READ;
		sim->execute(ins, addr);
	}
}

