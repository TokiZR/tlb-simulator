#include "paging.h"

#include <string.h>

#include <sys/mman.h>

/*******************************************************************************
 *
 *	Page Allocator.
 *
 */

PageAllocator::PageAllocator(tsize_t memSize, tsize_t base) {
	this->coremap = std::vector<bool>(memSize >> PAGE_BITS);
	this->baseaddr = base;
	this->size = memSize >> PAGE_BITS;
	this->freePtr = 0;
	this->n_used = 0;
}

paddr_t PageAllocator::alloc() {
	paddr_t addr;
	if (this->freePtr < this->size) {
		this->coremap[this->freePtr] = 1;
		addr = (freePtr << PAGE_BITS) + this->baseaddr;
		findPage();
		n_used++;
		return addr;
	} else {
		/**
		 * No pages found. Coremap is full.
		 */
		return 0;
	}
}

void PageAllocator::free(paddr_t page) {
	/* Remove the base address */
	page -= this->baseaddr;
	tsize_t ind = page >> PAGE_BITS;
	if (coremap[ind]) {
		n_used--;
		coremap[ind] = 0;
	}
	if (freePtr == size) freePtr = ind;
}

uint64_t PageAllocator::numUsed() {
	return this->n_used;
}

void PageAllocator::findPage() {
	tsize_t start = freePtr;
	while (freePtr < size) {
		if (coremap[freePtr])
			freePtr++;
		else
			return;
	}
	freePtr = 0;
	while (freePtr < start) {
		if (coremap[freePtr])
			freePtr++;
		else
			return;
	}
	freePtr = size;
}

/*******************************************************************************
 *
 *	Abstract base class.
 *
 */

PageTable::PageTable(PageAllocator * pa) {
	this->pa = pa;
	this->zero.address = 0;
}

PageTable::~PageTable() {
}

/*******************************************************************************
 *
 *		Pentium 4 Page Table
 *
 */

#define DIR_PTR(dir) ((P4PageTable::Directory *) (size_t) (dir))

/**
 * Top level page table layer.
 */
struct P4PageTable::PageRoot {
#define ROOT_LENGTH (PAGE_SIZE / sizeof(vaddr_t))
	tsize_t dirent[ROOT_LENGTH];
};

struct P4PageTable::Directory {
#define DIR_LENGTH (PAGE_SIZE / sizeof(PTE))
	PTE pages[DIR_LENGTH];
};

P4PageTable::P4PageTable(PageAllocator * pa) :
		PageTable(pa) {
	unsigned i;
	root = new PageRoot();
	for (i = 0; i < ROOT_LENGTH; i++) {
		/* The first page is *never* mapped for a given process. That means we
		 * can use 0 as null. */
		root->dirent[i] = 0;
	}
}

P4PageTable::~P4PageTable() {
	for (unsigned i = 0; i < ROOT_LENGTH; i++) {
		if (DIR_PTR(root->dirent[i])) {
			munmap(DIR_PTR(root->dirent[i]), PAGE_SIZE);
		}
	}
	delete root;
}

PTE& P4PageTable::getPage(vaddr_t vaddr) {
	/* Pete is all around invalid */
	tsize_t dirIndex = (vaddr >> 22) & 0x3FF;
	if (root->dirent[dirIndex]) {
		return DIR_PTR(root->dirent[dirIndex])->pages[(vaddr >> 12) & 0x3FF];
	}
	return zero;
}

void P4PageTable::mapPage(vaddr_t vaddr, bool write, bool execute) {
	paddr_t p;
	PTE * pete;
	tsize_t dirIndex = (vaddr >> 22) & 0x3FF;
	if (root->dirent[dirIndex] && DIR_PTR(root->dirent[dirIndex])->pages[(vaddr >> 12) & 0x3FF].valid) {
		dbg_print("Page 0x%" PRIts " is already mapped.\n", vaddr);
		return;
	} else {
		p = pa->alloc();
		if (!root->dirent[dirIndex]) {
			/* mmap clears the provided buffer for security reasons which
			 * results in all entries having valid = 0.
			 *
			 * Since we only allocate up to 1024 pages there is little risk of
			 * running out of pages in the 32bit range.
			 */
			root->dirent[dirIndex] = (tsize_t) (size_t) mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE,
			MAP_32BIT | MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
		}

		pete = &DIR_PTR(root->dirent[dirIndex])->pages[(vaddr >> 12) & 0x3FF];
		pete->access = pete->dirty = 0;
		pete->valid = 1;
		pete->present = 1;
		pete->execute = execute;
		pete->write = write;
		pete->paddr = p >> PAGE_BITS;
	}
}

void P4PageTable::unmapPage(vaddr_t page) {
	PTE& pete = getPage(page);
	/* Currently we do not try to free directory pages */
	pete.valid = 0;
}

void P4PageTable::clear() {
	for (unsigned i = 0; i < ROOT_LENGTH; i++) {
		if (root->dirent[i]) munmap((void *) (size_t) root->dirent[i], PAGE_SIZE);
	}
}

