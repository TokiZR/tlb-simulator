#include "def.h"

const char * instructionToString(Instruction in) {
	switch (in) {
		case INSTRUCTION_READ:
			return "R";
		case INSTRUCTION_WRITE:
			return "W";
		case INSTRUCTION_IFETCH:
			return "IF";
		case INSTRUCTION_INVALIDATE_CACHE:
			return "FC";
		case INSTRUCTION_INVALIDATE_CACHE_ADDRESS:
			return "FA";
		case INSTRUCTION_INVALIDATE_TLB:
			return "FT";
		case INSTRUCTION_CLEAR_TP:
			return "IAS";
		default:
			return "Invalid instruction code.";
	}
}

const char * pageFaultToString(PageFault f) {
	switch (f) {
		case PAGE_FAULT_WRITE:
			return "Write Permission Fault";
			break;
		case PAGE_FAULT_READ:
			return "Read Permission Fault";
			break;
		case PAGE_FAULT_EXECUTE:
			return "Execute Permission Fault";
			break;
		case PAGE_FAULT_UNMAPPED:
			return "Unmapped Page";
			break;
		default:
			return "Invalid page fault code!";
	}
}

TranslationLog::TranslationLog(FILE * out) {
	this->stream = out;
}

void TranslationLog::translation(vaddr_t vaddr, paddr_t paddr, Instruction in) {
	fprintf(stream, "%s: V(%" PRIts ") -> P(%" PRIts ")\n", instructionToString(in), vaddr, paddr);
}

void TranslationLog::pageFault(vaddr_t vaddr, PageFault cause) {
	fprintf(stream, "Page Fault at V(%" PRIts "), %s.\n", vaddr, pageFaultToString(cause));
}

void TranslationLog::pageAlloc(vaddr_t vpage, paddr_t ppage) {
	fprintf(stream, "Created new page mapping from V(%" PRIts "), to P(%" PRIts ").\n", vpage, ppage);
}
