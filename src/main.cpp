#include <stdio.h>
#include <stdlib.h>

#include <getopt.h>

#include "din-reader.h"
#include "simulation.h"

int logbase2(uint64_t no) {
	asm ("bsr %1, %0"
			: "=r"(no)
			: "r"(no)
			:);
	return no;
}

int main(int argc, char ** argv) {
	uint32_t mem_base = 0x400000, mem_size = 512 * 1024 * 1024;
	uint16_t tlb_size = 256;

	const char * options = "hb:s:t:o:";
	char opt;

	FILE * f, *out = stdout;

	/**
	 * Getting the command line arguments.
	 */
	while ((opt = getopt(argc, argv, options)) > 0) {
		switch (opt) {
			case 'h':
				puts(R"(SYNOPSIS:
	vmsim [OPTION]... [FILE]

DESCRIPTION
	Simulate the tlb for memory access trace in DineroII format. The trace is read from the provided file or the standard input if no file is provided.

OPTIONS:
 -b: Base address of the memory pool (cosmetic).
 -s: Size of the memory pool (max address = base + size).
 -t: Size of the tlb (should be a power of two).
 -o: Output file (default is stdout).
)");
				exit(0);
				break;
			case 'b':
				mem_base = atoi(optarg);
				break;
			case 's':
				mem_size = atoi(optarg) * PAGE_SIZE;
				break;
			case 't':
				tlb_size = atoi(optarg);
				if (1 << logbase2(tlb_size) != tlb_size) {
					fprintf(stderr, "The size of the TLB should be a power of two.\n");
					exit(1);
				}
				break;
			case 'o':
				out = fopen(optarg, "w");
				if (!out) {
					perror("Could not open output file");
					exit(1);
				}
				break;
			default:
				exit(1);
		}
	}

	if (mem_base + mem_size <= mem_base) {
		fprintf(stderr, "Base address plus memory size overflows the physical address space.\n");
		exit(1);
	}

	if (argc > optind) {
		f = fopen(argv[optind], "r");
		if (!f) {
			perror("Could not open input file");
			exit(1);
		}
	} else {
		f = stdin;
	}

	printf("Starting simulation:\n"
			"TLB size: %u\n"
			"Physical Memory Settings: 0x%x(base) + %ubytes\n", tlb_size, mem_base, mem_size);

	Simulation * sim = new Simulation(tlb_size, mem_base, mem_size, NULL, out);
	din_reader_read(f, sim);

	SimulationCounters ctrs;
	sim->getCounters(&ctrs);

	printf("\nTotal memory mapped: %" PRIu64 "\n"
			"Total memory references: %" PRIu64 "\n", ctrs.n_pages, ctrs.instructions);

	printf("\nTLB Performance Counters:\n"
			"Evictions: %" PRIu64 "\n"
			"Read hit rate: %05.3lf%%\n"
			"Write hit rate: %05.3lf%%\n"
			"Instruction fetch hit rate: %05.3lf%%\n",
			ctrs.evictions,
			(double) 100.0 * ctrs.read.hm_hits / (ctrs.read.hm_hits + ctrs.read.hm_misses),
			(double) 100.0 * ctrs.write.hm_hits / (ctrs.write.hm_hits + ctrs.write.hm_misses),
			(double) 100.0 * ctrs.ifetch.hm_hits / (ctrs.ifetch.hm_hits + ctrs.ifetch.hm_misses)
					);

	return 0;
}
