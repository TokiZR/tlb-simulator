#include "tlb.h"

#include <stdexcept>

/*******************************************************************************
 *
 * TLB Exception
 *
 */

TLBException::TLBException(PageFault f) {
	this->fault = f;
}

const char* TLBException::what() const throw () {
	switch (this->fault) {
		case PAGE_FAULT_WRITE:
			return "Memory access caused a WRITE_FAULT.";
			break;
		case PAGE_FAULT_READ:
			return "Memory access caused a READ_FAULT.";
			break;
		case PAGE_FAULT_EXECUTE:
			return "Memory access caused a EXECUTE_FAULT.";
			break;
		case PAGE_FAULT_UNMAPPED:
			return "Memory access caused a PAGE_FAULT.";
			break;
		default:
			return "Invalid exception!!!!!";
	}
}

PageFault TLBException::cause() {
	return this->fault;
}

/*******************************************************************************
 *
 * TLB abstract base class.
 *
 */

TLB::TLB(PageTable * table, uint16_t tlbsize) {
	hitmiss zero = {0, 0};
	this->ifetch = this->read = this->write = zero;

	this->pageTable = table;
	this->tlbsize = tlbsize;
	this->evictions = 0;
}

TLB::~TLB() {
}

void TLB::setPageTable(PageTable * pt) {
	this->pageTable = pt;
	this->invalidate();
}

void TLB::reset() {
	hitmiss zero = {0, 0};
	this->evictions = 0;
	this->write = this->ifetch = this->read = zero;
	invalidate();
}

void TLB::getStatistics(hitmiss& read, hitmiss& write, hitmiss& ifetch, uint64_t& evictions) {
	read = this->read;
	write = this->write;
	ifetch = this->ifetch;
	evictions = this->evictions;
}

struct SimpleTLB::TLBEntry {
	/* Pete puts price tags on the supermarket */
	vaddr_t tag;
	/* Pete is a nice guy */
	PTE pete;
};

SimpleTLB::SimpleTLB(PageTable * table, uint16_t tlbsize) :
		TLB(table, tlbsize) {
	TLBEntry zero;
	zero.tag = 0;
	zero.pete.address = 0;
	clockHand = 0;
	this->table = new std::unordered_map<vaddr_t, TLBEntry*>();
	this->entries = new TLBEntry[tlbsize];
	for (int i = 0; i < tlbsize; i++) {
		this->entries[i] = zero;
	}
}

SimpleTLB::~SimpleTLB() {
	delete this->table;
	delete this->entries;
}

void SimpleTLB::printmap() {
	for(auto kv : *this->table) {
		printf("Key %x: %x\n", kv.first, kv.second->tag);
	}
}

paddr_t SimpleTLB::get(vaddr_t vaddr, Instruction i) throw (TLBException) {
	vaddr_t tag = GET_PAGE(vaddr);
	TLBEntry * entry;
	bool hit = 1;
	entry = (*table)[tag];
	if (!entry) {
		hit = 0;
		/* Have to evict stuff. */
		PTE pete = this->pageTable->getPage(tag);
		if (pete.valid) {
			entry = this->findVictim();
			entry->tag = tag;
			entry->pete = pete;
			(*table)[tag] = entry;
			//this->printmap();
		} else {
			/* Page Fault!!!!!! */
			entry = NULL;
		}
	}

	/* Record statistics first */
	if (hit) {
		switch (i) {
			case INSTRUCTION_READ:
				this->read.hm_hits++;
				break;
			case INSTRUCTION_WRITE:
				this->write.hm_hits++;
				break;
			case INSTRUCTION_IFETCH:
				this->ifetch.hm_hits++;
				break;
			default:
				break;
		}
	} else {
		switch (i) {
			case INSTRUCTION_READ:
				this->read.hm_misses++;
				break;
			case INSTRUCTION_WRITE:
				this->write.hm_misses++;
				break;
			case INSTRUCTION_IFETCH:
				this->ifetch.hm_misses++;
				break;
			default:
				break;
		}
	}

	/* if pete is not set we have a pagefault (unmapped page) */
	if (!entry) {
		throw TLBException(PAGE_FAULT_UNMAPPED);
	} else {
		entry->pete.access = 1;
		return (entry->pete.paddr << PAGE_BITS) | (vaddr & 0xFFF);
	}
}

void SimpleTLB::invalidate(vaddr_t vaddr) {

}

void SimpleTLB::invalidate() {

}

SimpleTLB::TLBEntry * SimpleTLB::findVictim(void) {
	TLBEntry * e;

	/* One bit clock */
	for (;; clockHand = (clockHand + 1) % this->tlbsize) {
		e = this->entries + clockHand;
		if (!e->pete.valid) goto end;
		if (!e->pete.access) {
			evictions++;
			goto end;
		} else
			e->pete.access = 0;
	}

	/* Increment clock hand */
	clockHand = (clockHand + 1) % this->tlbsize;

	this->evictions++;

	end:
	if(e->pete.valid) table->erase(e->tag);
	return e;
}
