#include "simulation.h"

#include <string.h>

Simulation::Simulation(uint16_t tlb_size, uint32_t mem_base, uint32_t mem_size, PageTable * pt, FILE * log) {
	memset(&this->ctrs, 0, sizeof(this->ctrs));
	this->log = TranslationLog(log);

	if (pt) {
		this->pt = pt;
		this->dpc = 0;
	} else {
		this->dpc = 1;
		this->pa = new PageAllocator(mem_size, mem_base);
		this->pt = new P4PageTable(this->pa);

	}

	this->tlb = new SimpleTLB(this->pt, tlb_size);
}

void Simulation::prepare() {
	memset(&this->ctrs, 0, sizeof(this->ctrs));
}

void Simulation::execute(Instruction in, tsize_t addr) {
	/* In this case the addr argument is used. */
	paddr_t paddr;
	if (in < INSTRUCTION_MEMOP_MAX) {
		this->ctrs.instructions++;
		try {
			paddr = this->tlb->get(addr, in);
		} catch (TLBException &e) {
			/* If we have DPC enabled we map a new page, otherwise we just
			 * ignore the fault since the recording happens at the tlb. */
			if (dpc) {
				pt->mapPage(GET_PAGE(addr), 1, 1);
				paddr = this->tlb->get(addr, in);
				log.pageAlloc(GET_PAGE(addr), GET_PAGE(paddr));
			} else {
				log.pageFault(addr, e.cause());
				this->ctrs.pagefaults++;
				return;
			}
		}
		log.translation(addr, paddr, in);
	}
}

void Simulation::getCounters(SimulationCounters * ctrs) {
	this->ctrs.n_pages = this->pa->numUsed();
	tlb->getStatistics(this->ctrs.read, this->ctrs.write, this->ctrs.ifetch, this->ctrs.evictions);
	*ctrs = this->ctrs;
}
