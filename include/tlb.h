#ifndef __TLB_H__
#define __TLB_H__ 1

#include "def.h"
#include "paging.h"

#include <unordered_map>
#include <exception>

class TLBException: public std::exception {
public:
	TLBException(PageFault f);

	const char* what() const throw ();
	PageFault cause();

private:
	PageFault fault;
};

/**
 * Translation Lookaside Buffer.
 *
 * This is a interface.
 */
class TLB {
public:
	/**
	 * TLB constructor.
	 * @param table page table this tlb will ask for mappings.
	 * @param tlbsize the number of entries on the tlb, ideally a power of two.
	 */
	TLB(PageTable * table, uint16_t tlbsize);
	virtual ~TLB();

	/**
	 * Gets a physical page address from a virtual address.
	 *
	 * The tlb will automatically go to the page table to get a new mapping and
	 * evict entries and so forth.
	 *
	 * @param vaddr target virtual address
	 * @param i Instruction that originates the request, for profiling purposes.
	 * @return resulting physical page.
	 */
	virtual paddr_t get(vaddr_t vaddr, Instruction i) throw (TLBException) = 0;

	/**
	 * Invalidate a given virtual page.
	 * @param vaddr page address
	 */
	virtual void invalidate(vaddr_t vaddr) = 0;

	/**
	 * Invalidate the entire TLB.
	 */
	virtual void invalidate() = 0;

	/**
	 * Change the current page table, only changes the pointer, does not cause
	 * a tlb invalidation.
	 * @param pt new page table pointer.
	 */
	void setPageTable(PageTable * pt);

	/**
	 * Get the operation statistics for the tlb.
	 * @param read Read statistics.
	 * @param write Write statistics
	 * @param ifetch IFetch statistics.
	 * @param pagefaults Number of pagefaults.
	 */
	void getStatistics(hitmiss& read, hitmiss& write, hitmiss& ifetch, uint64_t& evictions);

	/**
	 * Resets the statistics and invalidates the TLB.
	 */
	void reset();

protected:
	/**
	 * Read access counter.
	 */
	hitmiss read;
	/**
	 * Write access counter
	 */
	hitmiss write;
	/**
	 * IFetch access counter.
	 */
	hitmiss ifetch;

	/**
	 * Number of valid entry evictions.
	 */
	uint64_t evictions;

	/**
	 * Page table.
	 */
	PageTable * pageTable;

	/**
	 * Size of the tlb.
	 */
	uint16_t tlbsize;
};

class SimpleTLB : public TLB {
public:
	SimpleTLB(PageTable * table, uint16_t tlbsize);
	~SimpleTLB();

	paddr_t get(vaddr_t vaddr, Instruction i) throw (TLBException);
	void invalidate(vaddr_t vaddr);
	void invalidate();

private:
	/**
	 * An opaque tlb entry.
	 */
	struct TLBEntry;

	/**
	 * Evicts an entry using the clock replacement policy.
	 * This already sets the eviction counts.
	 *
	 * @return The entry that was selected to be replaced.
	 */
	TLBEntry * findVictim();

	void printmap();

	/**
	 * Mapping of virtual addresses to entries is done through a hashtable.
	 */
	std::unordered_map<vaddr_t, TLBEntry *> * table;

	/**
	 * The Entries are kept on a list of tlb entries.
	 */
	TLBEntry * entries;

	/**
	 * The clock hand.
	 */
	uint16_t clockHand;
};

#endif
