#ifndef __DEF_H__
#define __DEF_H__ 1

#include <stdint.h>
#include <stddef.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#ifdef SIM64

#define TARGET_WORDSIZE 64
#define PAGE_ADDR_SIZE 52
#define PAGE_MASK (~0xFFFL)

typedef uint64_t tsize_t;
#define PRIts PRIx64

#else

#define TARGET_WORDSIZE 32
#define PAGE_ADDR_SIZE 20
#define PAGE_MASK (~0xFFF)

typedef uint32_t tsize_t;
#define PRIts PRIx32

#endif

/**
 * Virtual address 0 is never mapped and physical address 0 is BIOS code so
 * 0 (NULL) is an invalid address in any context.
 */

/**
 * Virtual address.
 */
typedef tsize_t vaddr_t;

/**
 * Physical addresses.
 */
typedef tsize_t paddr_t;

#define PAGE_BITS 12
#define PAGE_SIZE (1 << PAGE_BITS)

#define GET_PAGE(addr) (addr & PAGE_MASK)

/*******************************************************************************
 *
 * Debug
 *
 */
#include <stdio.h>

#ifndef NODEBUG
#define DEBUG
#endif

#ifdef DEBUG
#define dbg_print(format, ...) do {\
		printf(format, ##__VA_ARGS__);\
	} while(0)
#else
#define dbg_print(format, ...)
#endif

/*******************************************************************************
 *
 * 							Memory Hierarchy Information
 *
 */

/**
 * Instructions to the memory hierarchy.
 */
enum Instruction {
	INSTRUCTION_READ,                    //!< A memory read (R).
	INSTRUCTION_WRITE,                   //!< A memory write (W).
	INSTRUCTION_IFETCH,                  //!< Read due to instruction fetch (IF).
	INSTRUCTION_MEMOP_MAX,               //!< Maximum index for memory operations.

	INSTRUCTION_INVALIDATE_CACHE,        //!< Invalidate the whole cache (FC, Flush Cache).
	INSTRUCTION_INVALIDATE_CACHE_ADDRESS,        //!< Invalidate a address on the cache (FA, Flush Address).
	INSTRUCTION_INVALIDATE_TLB,          //!< Invalidate the entire TLB (FT, Flush Tlb).
	INSTRUCTION_CLEAR_TP,                //!< Clear the page table (IAS, Invalidate Address Space).
	INSTRUCTION_DIRECTIVE_MAX            //!< Maximum for unit directives.
};

/**
 * Converts a instruction value to a compact string representation
 * @param in Instruction to convert.
 * @return short character code describing the instruction.
 */
const char * instructionToString(Instruction in);

enum PageFault {
	PAGE_FAULT_WRITE,
	PAGE_FAULT_READ,
	PAGE_FAULT_EXECUTE,
	PAGE_FAULT_UNMAPPED
};

/**
 * Convert a page fault code into a human readable string.
 * @param f Fault code.
 * @return Code description.
 */
const char * pageFaultToString(PageFault f);

/*******************************************************************************
 *
 * 	Data structs.
 *
 */

struct hitmiss {
	uint64_t hm_hits, hm_misses;
};

/**
 * System wide log for translation events.
 */
class TranslationLog {
public:
	TranslationLog(FILE * out = stdout);

	/**
	 * Used to log a successful translation.
	 * @param vaddr Original virtual address.
	 * @param paddr Resulting Physical address.
	 * @param in Memory instruction in question.
	 */
	void translation(vaddr_t vaddr, paddr_t paddr, Instruction in);

	/**
	 * Logs a page fault.
	 * @param vaddr
	 */
	void pageFault(vaddr_t vaddr, PageFault cause);

	/**
	 * Page allocation.
	 */
	void pageAlloc(vaddr_t vpage, paddr_t ppage);

	/**
	 * Output stream for the translation log.
	 */
	FILE * stream;
};

#endif
