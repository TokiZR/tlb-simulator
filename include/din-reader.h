#ifndef __DIN_READER_H__
#define __DIN_READER_H__

#include <stdio.h>
#include "simulation.h"

/**
 * Reads a memory reference trace in DineroII format and executes every
 * instruction there in a given simulation instance.
 */
void din_reader_read(FILE * din, Simulation * sim);

#endif
