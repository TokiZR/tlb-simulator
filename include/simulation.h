#ifndef __SIMULATION_H__
#define __SIMULATION_H__ 1

#include "def.h"

#include "paging.h"
#include "tlb.h"

struct SimulationCounters {
	uint64_t instructions;
	uint64_t pagefaults;
	uint64_t total_pages;
	uint64_t evictions;
	uint64_t n_pages;
	hitmiss read;
	hitmiss write;
	hitmiss ifetch;
};

class Simulation {
public:
	/**
	 * Starts a new simulation with a given tlb size.
	 *
	 * If a page table is provided it will be used for translation.
	 *
	 * Otherwise automatic page creation will be enabled (which implies page faults
	 * will never happen).
	 */
	Simulation(uint16_t tlb_size, uint32_t mem_base, uint32_t mem_size, PageTable * pt = NULL, FILE * log = stdout);

	/**
	 * Clear internal counters, caches, and the PT.
	 */
	void prepare();

	/**
	 * Executes a single instruction in the simulation.
	 *
	 * Instructions are defined on the instruction enum.
	 */
	void execute(Instruction in, tsize_t addr = 0);

	/**
	 * Return the internal counters of the simulation.
	 */
	void getCounters(SimulationCounters * ctrs);

private:
	/**
	 * Translation lookaside buffer.
	 */
	TLB * tlb;

	/**
	 * Pointer to the page table.
	 */
	PageTable * pt;

	/**
	 * Page allocator.
	 */
	PageAllocator * pa;

	/**
	 * Counters
	 */
	SimulationCounters ctrs;

	TranslationLog log;

	/***********************************
	 * Flags
	 */

	/**
	 * Dynamic page creation.
	 */
	unsigned dpc :1;
};

#endif
