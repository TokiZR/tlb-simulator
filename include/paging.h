#ifndef __PAGING_H__
#define __PAGING_H__ 1

#include "def.h"

#include <vector>
#include <stdint.h>

class PageAllocator {
public:
	/**
	 * A simple page allocator.
	 *
	 * While memSize is the size of the ram (after base) in bytes it must be a
	 * multiple of the system page size.
	 *
	 * @param memSize the number of bytes of memory available.
	 * @param base the base address of the main memory. Should be page aligned.
	 */
	PageAllocator(tsize_t memSize, tsize_t base);

	/**
	 * Returns the next valid physical page.
	 */
	paddr_t alloc();

	/**
	 * Free a page.
	 * @param page Page to be released.
	 */
	void free(paddr_t page);

	/**
	 * Return the number of pages in use by the core map.
	 * @return Number of pages in use.
	 */
	uint64_t numUsed();

private:
	/**
	 * Finds a free page to park the free pointer.
	 */
	void findPage();

	/**
	 * The coremap stores one bit per memory page.
	 */
	std::vector<bool> coremap;

	/**
	 * The base address of all physical pages.
	 */
	tsize_t baseaddr;

	/**
	 * The number of physical pages.
	 */
	tsize_t size;

	/**
	 * A pointer to a page that is known to be free, if equal to size then the
	 * core map is full.
	 */
	tsize_t freePtr;

	/**
	 * Number of pages in use.
	 */
	tsize_t n_used;
};

/**
 * Page Table Entry
 */
union PTE {
	struct {
		/**
		 * Physical page address size.
		 */
		tsize_t paddr :PAGE_ADDR_SIZE;

		/**
		 * Page flags.
		 * Up to 12 flaggies.
		 */

		/**
		 * Permission flags
		 */

		/**
		 * Permission to write.
		 */
		unsigned write :1;

		/**
		 * Permission to execute.
		 */
		unsigned execute :1;

		/**
		 * Info flags.
		 */

		/**
		 * Has dirty dat not in disk.
		 */
		unsigned dirty :1;

		/**
		 * Is in ram.
		 */
		unsigned present :1;

		/**
		 * Is a valid entry.
		 */
		unsigned valid :1;

		/**
		 * Has been accessed recently.
		 * (used only on the TLB)
		 */
		unsigned access :1;
	};
	vaddr_t address;
};

class PageTable {
public:
	PageTable(PageAllocator * pa);
	virtual ~PageTable();

	/**
	 * Get a page table entry for a given virtual page.
	 * The page number is taken from the given vaddr so any address can be used
	 * and the lower 12 bits will be ignored.
	 *
	 * @param vaddr Virtual address to get PTE for.
	 * @return PTE for that given vaddr.
	 */
	virtual PTE& getPage(vaddr_t vaddr) = 0;

	/**
	 * Map a new page, the page will be allocated on the allocator provided
	 * at construction time.
	 *
	 * @param vaddr virtual address of the new page.
	 * @param write Weather the page will have write permission.
	 * @param execute Weather the page will have execution permission.
	 */
	virtual void mapPage(vaddr_t vaddr, bool write, bool execute) = 0;

	/**
	 * Unmaps a virtual page.
	 * @param page The page to be unmapped.
	 */
	virtual void unmapPage(vaddr_t page) = 0;

	/**
	 * Clear the page table.
	 */
	virtual void clear() = 0;

protected:
	/**
	 * This keeps the physical pages this page table referewnces.
	 */
	PageAllocator * pa;

	/**
	 * This stub PTE will be returned when a page is not only not mapped but no
	 * PTE exists for it (which happes for instance when the directory is
	 * empty).
	 */
	PTE zero;
};

/**
 * Pentium 4 two level page table.
 *
 * This is a 32-bit page table and will not work out for 64-bit targets.
 * For 64-bit use the Opteron Page table.
 */
class P4PageTable: public PageTable {
public:

	P4PageTable(PageAllocator * pa);
	virtual ~P4PageTable();

	PTE& getPage(vaddr_t vaddr);
	void mapPage(vaddr_t vaddr, bool write, bool execute);
	void unmapPage(vaddr_t page);
	void clear();

private:
	struct PageRoot;
	struct Directory;

	/**
	 * Top level of the page table hierarchy.
	 */
	PageRoot * root;
};

#endif
