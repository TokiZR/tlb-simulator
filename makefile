###############################################################################
# 
# Configurables.
#

#Using an auto incrementing build number.
BUILDNO_FILE = .buildno
BUILD_NUMBER = $(shell cat $(BUILDNO_FILE))

# Argument to pass to the compiler to prompt it to generate dependencies.
CDEP_FLAGS := -MM

# Directories
# Source directory
SRC := src
# Where headers are
INCLUDE := include
# Directory to store dependency files
DEPEND := build/dep
# Intermediate binaries directory
BIN := build/bin
# Built shared objects directory
LIB := lib
# Built modules location
MODBIN := modules/bin

# Generic compile and link flags, these get incremented from the command line
# as well :D
LDFLAGS ?= -g3
CFLAGS ?= -g3

export LDFLAGS
export CFLAGS

#Libs to be linked
LIBS +=
INCLUDES += -I$(INCLUDE) -Isrc
GCC_MISC += -D_GNU_SOURCE -std=c++11
WARNINGS += -Wall

FILES = main.cpp simulation.cpp din-reader.cpp paging.cpp tlb.cpp def.cpp

MAIN = vmsim

# Compiler paths
export CC = g++
export LD = g++

################################################################################
#
# Rules etc.
#

objects := $(addprefix $(BIN)/,$(FILES:.cpp=.o))
deps := $(addprefix $(DEPEND)/,$(FILES:.cpp=.d))

# Main rule
.PHONY : all
all : $(MAIN)

# Include dependency rules. Auto-generated during build
-include $(deps)

# Final binary
$(MAIN) : $(objects)
	$(LD) -o $(MAIN) $(objects) $(CFLAGS) $(LDFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS) $(LIBS)

# C rules, create new ones for other languages
$(objects): $(BIN)/%.o: $(SRC)/%.cpp
	@mkdir -p $(@D)
	@mkdir -p $(DEPEND)/$(dir $*)
	$(CC) -c -o $@ $(CFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS) $<
	@echo -n $(@D)/ > $(DEPEND)/$*.d
	@$(CC) $(CFLAGS) $(INCLUDES) $(GCC_MISC) $(WARNINGS) $(CDEP_FLAGS) >> $(DEPEND)/$*.d $<

# Clean
.PHONY : clean
clean :
	rm -rf $(MAIN) $(BIN) $(LIB) $(DEPEND) build


